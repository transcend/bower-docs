# Docs Module
The "Docs" module provides the styling, and base documentation content for other applications to build off to create a
consistent look and feel for documentation across applications. This library is the raw doc files before being "built".
This package is meant to be used in the build process for applications to build there own documentation. To se an
example of the "built" documentation see the Transcend Documentation: [docs.tsstools.com](http://docs.tsstools.com/)

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-docs.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-docs/get/master.zip](http://code.tsstools.com/bower-docs/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-docs](http://code.tsstools.com/bower-docs)
* If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/docs/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.docs']);
```

## To Do
- Hook the docs styling into the LESS build process.

## Project Goals
- Maintain a consistent, easy to maintain, base for documentation.